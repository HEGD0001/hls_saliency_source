#include "hls_opencv.h"
#include "color_map.h"
#define INPUT_IMAGE "test_img.jpg"

int main (int argc, char** argv) {


    IplImage* in_img = cvLoadImage(INPUT_IMAGE);


    IplImage* color_map = cvCreateImage(cvSize(in_img->width/2, in_img->height/2), in_img->depth, 1);
    IplImage * frame_r = cvCreateImage(cvGetSize(in_img), HLS_8U, 1);
    IplImage * frame_g = cvCreateImage(cvGetSize(in_img), HLS_8U, 1);
    IplImage * frame_b = cvCreateImage(cvGetSize(in_img), HLS_8U, 1);


    cvSplit(in_img, frame_r, frame_g, frame_b, NULL);

    AXI_STREAM  red_axi, green_axi, out_axi;
    IplImage2AXIvideo(frame_r, red_axi);
    IplImage2AXIvideo(frame_g, green_axi);

    hw_rg_map(red_axi, green_axi, out_axi, in_img->height, in_img->width);

    AXIvideo2IplImage(out_axi, color_map);

    cvSaveImage("rg_map.jpg", color_map);
    cvReleaseImage(&color_map);
    cvReleaseImage(&frame_r);
    cvReleaseImage(&frame_g);
    cvReleaseImage(&frame_b);

    return 0;

}
