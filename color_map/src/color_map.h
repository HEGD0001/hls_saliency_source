#ifndef _COLOR_MAP_H_
#define _COLOR_MAP_H_
#include "hls_video.h"

// maximum image size
#define MAX_WIDTH  1920
#define MAX_HEIGHT 1080

typedef hls::stream<ap_axiu<32,1,1,1> >               AXI_STREAM;
typedef hls::Mat<MAX_HEIGHT, MAX_WIDTH, HLS_8UC1>     	  GRAY_IMAGE;

void hw_rg_map(AXI_STREAM& red_axi, AXI_STREAM& green_axi, AXI_STREAM& rg_map_axi, int rows, int cols);
#endif // _COLOR_MAP_H_
