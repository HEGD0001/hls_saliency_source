#include "color_map.h"


void hw_color_map(AXI_STREAM& red_axi, AXI_STREAM& green_axi, AXI_STREAM& rg_map_axi, int rows, int cols)
{

#pragma HLS INTERFACE axis port=red_axi
#pragma HLS INTERFACE axis port=green_axi
#pragma HLS INTERFACE axis port=rg_map_axi


#pragma HLS RESOURCE core=AXI_SLAVE variable=rows metadata="-bus_bundle CONTROL_BUS"
#pragma HLS RESOURCE core=AXI_SLAVE variable=cols metadata="-bus_bundle CONTROL_BUS"
#pragma HLS RESOURCE core=AXI_SLAVE variable=return metadata="-bus_bundle CONTROL_BUS"

#pragma HLS INTERFACE ap_stable port=rows
#pragma HLS INTERFACE ap_stable port=cols

#pragma HLS dataflow
	GRAY_IMAGE red(rows, cols), green(rows, cols);
	GRAY_IMAGE rg_map(rows/2, cols/2);

	hls::AXIvideo2Mat(red_axi, red);
	hls::AXIvideo2Mat(green_axi, green);

	// red pyramid
	GRAY_IMAGE r_pyr_00(rows,cols),r_pyr_01(rows,cols);
	GRAY_IMAGE r_pyr_1(rows/2,cols/2), r_pyr_10(rows/2,cols/2), r_pyr_11(rows/2,cols/2);
	GRAY_IMAGE r_pyr_2(rows/4,cols/4), r_pyr_20(rows/4,cols/4), r_pyr_21(rows/4,cols/4);
	GRAY_IMAGE r_pyr_3(rows/8,cols/8), r_pyr_30(rows/8,cols/8), r_pyr_31(rows/8,cols/8);
	GRAY_IMAGE r_pyr_4(rows/16,cols/16);

	GRAY_IMAGE r_blur_img(rows, cols);

	hls::GaussianBlur<3,3>(red, r_blur_img);

	hls::Duplicate(r_blur_img, r_pyr_00,r_pyr_01);
	hls::Resize(r_pyr_00, r_pyr_1);
	hls::Duplicate(r_pyr_1, r_pyr_10, r_pyr_11);
	hls::Resize(r_pyr_10, r_pyr_2);
	hls::Duplicate(r_pyr_2, r_pyr_20, r_pyr_21);
	hls::Resize(r_pyr_20, r_pyr_3);
	hls::Duplicate(r_pyr_3, r_pyr_30, r_pyr_31);
	hls::Resize(r_pyr_30, r_pyr_4);

	// green pyramid
	GRAY_IMAGE g_pyr_00(rows,cols),g_pyr_01(rows,cols);
	GRAY_IMAGE g_pyr_1(rows/2, cols/2), g_pyr_10(rows/2, cols/2), g_pyr_11(rows/2, cols/2);
	GRAY_IMAGE g_pyr_2(rows/4, cols/4), g_pyr_20(rows/4, cols/4), g_pyr_21(rows/4, cols/4);
	GRAY_IMAGE g_pyr_3(rows/8, cols/8), g_pyr_30(rows/8, cols/8), g_pyr_31(rows/8, cols/8);
	GRAY_IMAGE g_pyr_4(rows/16, cols/16);

	GRAY_IMAGE g_blur_img(rows, cols);

	hls::GaussianBlur<3,3>(green, g_blur_img);

	hls::Duplicate(g_blur_img, g_pyr_00,g_pyr_01);
	hls::Resize(g_pyr_00, g_pyr_1);
	hls::Duplicate(g_pyr_1, g_pyr_10, g_pyr_11);
	hls::Resize(g_pyr_10, g_pyr_2);
	hls::Duplicate(g_pyr_2, g_pyr_20, g_pyr_21);
	hls::Resize(g_pyr_20, g_pyr_3);
	hls::Duplicate(g_pyr_3, g_pyr_30, g_pyr_31);
	hls::Resize(g_pyr_30, g_pyr_4);

	GRAY_IMAGE 		rg_diff_0(rows, cols);
	GRAY_IMAGE	rg_diff_1(rows/2, cols/2);
	GRAY_IMAGE	gr_diff_2(rows/4, cols/4);
	GRAY_IMAGE	gr_diff_3(rows/8, cols/8);
	GRAY_IMAGE	gr_diff_4(rows/16, cols/16);

	// find diff
	hls::AbsDiff(r_pyr_01, g_pyr_01, rg_diff_0);
	hls::AbsDiff(r_pyr_11, g_pyr_11, rg_diff_1);
	hls::AbsDiff(g_pyr_21, r_pyr_21, gr_diff_2);
	hls::AbsDiff(g_pyr_31, r_pyr_31, gr_diff_3);
	hls::AbsDiff(g_pyr_4,  r_pyr_4,  gr_diff_4);

	GRAY_IMAGE 		map_02(rows, cols), map_03(rows, cols), map_02_03(rows, cols), rg_diff_00(rows, cols), rg_diff_01(rows, cols), r_00(rows, cols), r_01(rows, cols);
	GRAY_IMAGE 	    map_13(rows/2 ,cols/2), map_14(rows/2 ,cols/2), map_13_14(rows/2 ,cols/2), map_1(rows/2 ,cols/2), rg_diff_10(rows/2 ,cols/2), rg_diff_11(rows/2 ,cols/2), r_10(rows/2 ,cols/2), r_11(rows/2 ,cols/2);
	GRAY_IMAGE	    gr_diff_30(rows/8, cols/8), gr_diff_31(rows/8, cols/8);
	// center surround
	hls::Duplicate(rg_diff_0, rg_diff_00, rg_diff_01);
	hls::Duplicate(rg_diff_1, rg_diff_10, rg_diff_11);
	hls::Duplicate(gr_diff_3, gr_diff_30, gr_diff_31);

	hls::Resize(gr_diff_2, r_00);
	hls::AbsDiff(r_00, rg_diff_00, map_02);

	hls::Resize(gr_diff_30, r_01);
	hls::AbsDiff(r_01, rg_diff_01, map_03);

	 hls::Resize(gr_diff_31, r_10);
	 hls::AbsDiff(r_10, rg_diff_10, map_13);

	hls::Resize(gr_diff_4, r_11);
	hls::AbsDiff(r_11, rg_diff_11, map_14);

	// combine maps
	hls::AddWeighted(map_02, 0.5, map_03, 0.5, 0.0, map_02_03);
	hls::AddWeighted(map_13, 0.5, map_14, 0.5, 0.0, map_13_14);

	hls::Resize(map_02_03, map_1);

	hls::AddWeighted(map_1, 0.5, map_13_14, 0.5, 0.0, rg_map);
	hls::Mat2AXIvideo(rg_map, rg_map_axi);

}
