------------Directory structure----------
- each directory has source files and scripts to build individual saliency blocks
- <dir>/Makefile ----> main makefile to build the ip and generate bitstream
- <dir>/src		-----> contains design and testbench source files for the ip
- <dir>/scripts -----> contains vivado HLS and Vivado scripts to generate ip and bitstream



---------------------Steps to build ip core-------------
- cd <dir>
- make hlp_ip ----> to build ip core and export
- make csim   ----> perform C simulation
- make cosim  ----> perform RTL cosimulation -- not working as of now
- make all    ----> build ip, export and generate bitstream -- not complete as it requires Vivado design suite script
