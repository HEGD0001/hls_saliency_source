#ifndef _IMG_SUB_H_
#define _IMG_SUB_H_

#include "hls_video.h"
#define MAX_HEIGHT  1080
#define MAX_WIDTH   1920


typedef hls::Mat<MAX_HEIGHT, MAX_WIDTH, HLS_8UC1>     GRAY_IMAGE;
typedef hls::stream<ap_axiu<32,1,1,1> >               AXI_STREAM;


void hw_img_sub(AXI_STREAM& img_1, AXI_STREAM& img_2, AXI_STREAM& output, int in_rows, int in_cols);



#endif // _IMG_SUB_H_
