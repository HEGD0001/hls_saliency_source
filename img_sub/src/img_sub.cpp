#include "img_sub.h"

#define MAX_HEIGHT  1080
#define MAX_WIDTH   1920


void hw_img_sub(AXI_STREAM& img_1, AXI_STREAM& img_2, AXI_STREAM& output, int in_rows, int in_cols)
{

//Create AXI streaming interfaces for the core
#pragma HLS INTERFACE axis port=img_1
#pragma HLS INTERFACE axis port=img_2
#pragma HLS INTERFACE axis port=output

#pragma HLS RESOURCE core=AXI_SLAVE variable=in_rows metadata="-bus_bundle CONTROL_BUS"
#pragma HLS RESOURCE core=AXI_SLAVE variable=in_cols metadata="-bus_bundle CONTROL_BUS"
#pragma HLS RESOURCE core=AXI_SLAVE variable=return metadata="-bus_bundle CONTROL_BUS"

#pragma HLS INTERFACE ap_stable port=in_rows
#pragma HLS INTERFACE ap_stable port=in_cols

    GRAY_IMAGE in_img1(in_rows, in_cols);
    GRAY_IMAGE in_img2(in_rows, in_cols);
    GRAY_IMAGE out_img(in_rows, in_cols);

    // Convert input image stream to Mat
    hls::AXIvideo2Mat(img_1, in_img1);
    hls::AXIvideo2Mat(img_2, in_img2);
    hls::AbsDiff(in_img1, in_img2, out_img);
    hls::Mat2AXIvideo(out_img, output);
}
