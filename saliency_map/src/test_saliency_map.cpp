#include "saliency_map.h"
#include "hls_opencv.h"

#define INPUT_IMAGE "test_img.jpg"

int main (int argc, char** argv) {


    IplImage* in_img = cvLoadImage(INPUT_IMAGE);


    IplImage* final_map = cvCreateImage(cvSize(in_img->width/2, in_img->height/2), in_img->depth, 1);

    AXI_STREAM  src_axi, out_axi;
    IplImage2AXIvideo(in_img, src_axi);

    hw_saliency_map(src_axi, out_axi, in_img->height, in_img->width);

    AXIvideo2IplImage(out_axi, final_map);

    cvSaveImage("saliency_map.jpg", final_map);
    cvReleaseImage(&final_map);

    return 0;

}
