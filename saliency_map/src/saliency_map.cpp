#include "saliency_map.h"

void hw_get_intensity_map(GRAY_IMAGE& int_img, GRAY_IMAGE& int_map, int rows,int cols);
void hw_get_rg_map( GRAY_IMAGE& red,  GRAY_IMAGE& green, GRAY_IMAGE& rg_map, int rows, int cols);

void hw_get_by_map( GRAY_IMAGE& red,  GRAY_IMAGE& green, GRAY_IMAGE& rg_map, int rows, int cols);
void hw_saliency_map(AXI_STREAM& INPUT_STREAM, AXI_STREAM& OUTPUT_STREAM, int rows, int cols)
{
    //Create AXI streaming interfaces for the core
#pragma HLS INTERFACE axis port=INPUT_STREAM
#pragma HLS INTERFACE axis port=OUTPUT_STREAM

#pragma HLS RESOURCE core=AXI_SLAVE variable=rows metadata="-bus_bundle CONTROL_BUS"
#pragma HLS RESOURCE core=AXI_SLAVE variable=cols metadata="-bus_bundle CONTROL_BUS"
#pragma HLS RESOURCE core=AXI_SLAVE variable=return metadata="-bus_bundle CONTROL_BUS"

#pragma HLS INTERFACE ap_stable port=rows
#pragma HLS INTERFACE ap_stable port=cols

	RGB_IMAGE in_img(rows, cols);
    GRAY_IMAGE int_img(rows, cols);
	GRAY_IMAGE red_ch(rows, cols);
	GRAY_IMAGE green_ch(rows, cols);
	GRAY_IMAGE blue_ch(rows, cols);
	GRAY_IMAGE yellow_ch(rows, cols);

    // Convert input image stream to Mat
	hls::AXIvideo2Mat(INPUT_STREAM, in_img);

	RGB_IMAGE in_img_copy1, in_img_copy2;
	hls::Duplicate(in_img, in_img_copy1, in_img_copy2);
	// Get intensity image
	hls::CvtColor<HLS_RGB2GRAY>(in_img_copy1, int_img);

	// Get R, G, B, Y chanels
	hls::Split(in_img_copy2, red_ch, green_ch, blue_ch);

	// Lets assume that Y = R + G /2 for simplicity
	hls::AddWeighted(red_ch,0.5, green_ch,0.5, 0.0, yellow_ch);

	// build intensity pyramid
	GRAY_IMAGE int_map(rows/2, cols/2),rg_map(rows/2, cols/2), by_map(rows/2, cols/2);
	GRAY_IMAGE norm_int_map(rows/2, cols/2),norm_rg_map(rows/2, cols/2), norm_by_map(rows/2, cols/2);
	hw_get_intensity_map(int_img, int_map, rows, cols);

	hw_get_rg_map(red_ch, green_ch, rg_map, rows, cols);
	hw_get_by_map(blue_ch,yellow_ch, by_map, rows, cols);

	// map normalization
	hls::EqualizeHist(int_map , norm_int_map);
	hls::EqualizeHist(int_map , norm_int_map);
	hls::EqualizeHist(int_map , norm_int_map);

	GRAY_IMAGE final_map(rows/2, cols/2), color_map(rows/2, cols/2);
	hls::AddWeighted(norm_rg_map, 0.5, norm_by_map, 0.5, 0.0, color_map);
	hls::AddWeighted(norm_int_map, 0.5, color_map, 0.5, 0.0, final_map);

    hls::Mat2AXIvideo(final_map, OUTPUT_STREAM);
}

void hw_get_intensity_map(GRAY_IMAGE& int_img, GRAY_IMAGE& int_map,  int rows, int cols)
{
    GRAY_IMAGE pyr_00(rows,cols), pyr_01(rows,cols),pyr_02(rows,cols),pyr_03(rows,cols), r_00(rows,cols),r_01(rows,cols);
    GRAY_IMAGE pyr_1(rows/2, cols/2), pyr_10(rows/2, cols/2), pyr_11(rows/2, cols/2), pyr_12(rows/2, cols/2), pyr_13(rows/2, cols/2), r_10(rows/2, cols/2), r_11(rows/2, cols/2);
    GRAY_IMAGE  pyr_2(rows/4, cols/4), pyr_20(rows/4, cols/4), pyr_21(rows/4, cols/4);
    GRAY_IMAGE pyr_3(rows/8, cols/8), pyr_30(rows/8, cols/8), pyr_31(rows/8, cols/8), pyr_32(rows/8, cols/8), pyr_33(rows/8, cols/8);
    GRAY_IMAGE pyr_4(rows/16,cols/16);

    GRAY_IMAGE blur_img(rows,cols);

    hls::GaussianBlur<3,3>(int_img, blur_img);

    hls::Duplicate(blur_img, pyr_00,pyr_01);
    hls::Resize(pyr_00, pyr_1);
    hls::Duplicate(pyr_1, pyr_10, pyr_11);
    hls::Resize(pyr_10, pyr_2);
    hls::Duplicate(pyr_2, pyr_20, pyr_21);
    hls::Resize(pyr_20, pyr_3);
    hls::Duplicate(pyr_3, pyr_30, pyr_31);
    hls::Resize(pyr_30, pyr_4);

    // find CS difference and intensity map
    GRAY_IMAGE map_02(rows,cols), map_03(rows,cols),map_02_03(rows,cols);
    GRAY_IMAGE map_13(rows/2,cols/2), map_14(rows/2,cols/2),map_1(rows/2,cols/2), map_13_14(rows/2,cols/2);

    hls::Resize(pyr_21, r_00);
    hls::Duplicate(pyr_01, pyr_02, pyr_03);
    hls::AbsDiff(r_00, pyr_02,map_02);

    hls::Duplicate(pyr_31, pyr_32, pyr_33);
    hls::Resize(pyr_32, r_01);
    hls::AbsDiff(r_01, pyr_03, map_03);

    hls::Resize(pyr_33, r_10);
    hls::Duplicate(pyr_11, pyr_12, pyr_13);
    hls::AbsDiff(r_10, pyr_12, map_13);

    hls::Resize(pyr_4, r_11);
    hls::AbsDiff(r_11,pyr_13,  map_14);
    // combine maps
    hls::AddWeighted(map_02, 0.5, map_03, 0.5, 0.0, map_02_03);
    hls::AddWeighted(map_13, 0.5, map_14, 0.5, 0.0, map_13_14);

    hls::Resize(map_02_03, map_1);

    hls::AddWeighted(map_1, 0.5, map_13_14, 0.5, 0.0, int_map);


}

void hw_get_rg_map( GRAY_IMAGE& red,  GRAY_IMAGE& green, GRAY_IMAGE& rg_map, int rows, int cols)
{
    // red pyramid
    GRAY_IMAGE r_pyr_00(rows,cols),r_pyr_01(rows,cols);
    GRAY_IMAGE r_pyr_1(rows/2,cols/2), r_pyr_10(rows/2,cols/2), r_pyr_11(rows/2,cols/2);
    GRAY_IMAGE r_pyr_2(rows/4,cols/4), r_pyr_20(rows/4,cols/4), r_pyr_21(rows/4,cols/4);
    GRAY_IMAGE r_pyr_3(rows/8,cols/8), r_pyr_30(rows/8,cols/8), r_pyr_31(rows/8,cols/8);
    GRAY_IMAGE r_pyr_4(rows/16,cols/16);

    GRAY_IMAGE r_blur_img(rows, cols);

    hls::GaussianBlur<3,3>(red, r_blur_img);

    hls::Duplicate(r_blur_img, r_pyr_00,r_pyr_01);
    hls::Resize(r_pyr_00, r_pyr_1);
    hls::Duplicate(r_pyr_1, r_pyr_10, r_pyr_11);
    hls::Resize(r_pyr_10, r_pyr_2);
    hls::Duplicate(r_pyr_2, r_pyr_20, r_pyr_21);
    hls::Resize(r_pyr_20, r_pyr_3);
    hls::Duplicate(r_pyr_3, r_pyr_30, r_pyr_31);
    hls::Resize(r_pyr_30, r_pyr_4);

    // green pyramid
    GRAY_IMAGE g_pyr_00(rows,cols),g_pyr_01(rows,cols);
    GRAY_IMAGE g_pyr_1(rows/2, cols/2), g_pyr_10(rows/2, cols/2), g_pyr_11(rows/2, cols/2);
    GRAY_IMAGE g_pyr_2(rows/4, cols/4), g_pyr_20(rows/4, cols/4), g_pyr_21(rows/4, cols/4);
    GRAY_IMAGE g_pyr_3(rows/8, cols/8), g_pyr_30(rows/8, cols/8), g_pyr_31(rows/8, cols/8);
    GRAY_IMAGE g_pyr_4(rows/16, cols/16);

    GRAY_IMAGE g_blur_img(rows, cols);

    hls::GaussianBlur<3,3>(green, g_blur_img);

    hls::Duplicate(g_blur_img, g_pyr_00,g_pyr_01);
    hls::Resize(g_pyr_00, g_pyr_1);
    hls::Duplicate(g_pyr_1, g_pyr_10, g_pyr_11);
    hls::Resize(g_pyr_10, g_pyr_2);
    hls::Duplicate(g_pyr_2, g_pyr_20, g_pyr_21);
    hls::Resize(g_pyr_20, g_pyr_3);
    hls::Duplicate(g_pyr_3, g_pyr_30, g_pyr_31);
    hls::Resize(g_pyr_30, g_pyr_4);

    GRAY_IMAGE      rg_diff_0(rows, cols);
    GRAY_IMAGE  rg_diff_1(rows/2, cols/2);
    GRAY_IMAGE  gr_diff_2(rows/4, cols/4);
    GRAY_IMAGE  gr_diff_3(rows/8, cols/8);
    GRAY_IMAGE  gr_diff_4(rows/16, cols/16);

    // find diff
    hls::AbsDiff(r_pyr_01, g_pyr_01, rg_diff_0);
    hls::AbsDiff(r_pyr_11, g_pyr_11, rg_diff_1);
    hls::AbsDiff(g_pyr_21, r_pyr_21, gr_diff_2);
    hls::AbsDiff(g_pyr_31, r_pyr_31, gr_diff_3);
    hls::AbsDiff(g_pyr_4,  r_pyr_4,  gr_diff_4);

    GRAY_IMAGE      map_02(rows, cols), map_03(rows, cols), map_02_03(rows, cols), rg_diff_00(rows, cols), rg_diff_01(rows, cols), r_00(rows, cols), r_01(rows, cols);
    GRAY_IMAGE      map_13(rows/2 ,cols/2), map_14(rows/2 ,cols/2), map_13_14(rows/2 ,cols/2), map_1(rows/2 ,cols/2), rg_diff_10(rows/2 ,cols/2), rg_diff_11(rows/2 ,cols/2), r_10(rows/2 ,cols/2), r_11(rows/2 ,cols/2);
    GRAY_IMAGE      gr_diff_30(rows/8, cols/8), gr_diff_31(rows/8, cols/8);
    // center surround
    hls::Duplicate(rg_diff_0, rg_diff_00, rg_diff_01);
    hls::Duplicate(rg_diff_1, rg_diff_10, rg_diff_11);
    hls::Duplicate(gr_diff_3, gr_diff_30, gr_diff_31);

    hls::Resize(gr_diff_2, r_00);
    hls::AbsDiff(r_00, rg_diff_00, map_02);

    hls::Resize(gr_diff_30, r_01);
    hls::AbsDiff(r_01, rg_diff_01, map_03);

     hls::Resize(gr_diff_31, r_10);
     hls::AbsDiff(r_10, rg_diff_10, map_13);
     hls::Resize(gr_diff_4, r_11);
     hls::AbsDiff(r_11, rg_diff_11, map_14);

     // combine maps
     hls::AddWeighted(map_02, 0.5, map_03, 0.5, 0.0, map_02_03);
     hls::AddWeighted(map_13, 0.5, map_14, 0.5, 0.0, map_13_14);

     hls::Resize(map_02_03, map_1);

     hls::AddWeighted(map_1, 0.5, map_13_14, 0.5, 0.0, rg_map);

}

void hw_get_by_map( GRAY_IMAGE& red,  GRAY_IMAGE& green, GRAY_IMAGE& rg_map, int rows, int cols)
{
    // red pyramid
    GRAY_IMAGE r_pyr_00(rows,cols),r_pyr_01(rows,cols);
    GRAY_IMAGE r_pyr_1(rows/2,cols/2), r_pyr_10(rows/2,cols/2), r_pyr_11(rows/2,cols/2);
    GRAY_IMAGE r_pyr_2(rows/4,cols/4), r_pyr_20(rows/4,cols/4), r_pyr_21(rows/4,cols/4);
    GRAY_IMAGE r_pyr_3(rows/8,cols/8), r_pyr_30(rows/8,cols/8), r_pyr_31(rows/8,cols/8);
    GRAY_IMAGE r_pyr_4(rows/16,cols/16);

    GRAY_IMAGE r_blur_img(rows, cols);

    hls::GaussianBlur<3,3>(red, r_blur_img);

    hls::Duplicate(r_blur_img, r_pyr_00,r_pyr_01);
    hls::Resize(r_pyr_00, r_pyr_1);
    hls::Duplicate(r_pyr_1, r_pyr_10, r_pyr_11);
    hls::Resize(r_pyr_10, r_pyr_2);
    hls::Duplicate(r_pyr_2, r_pyr_20, r_pyr_21);
    hls::Resize(r_pyr_20, r_pyr_3);
    hls::Duplicate(r_pyr_3, r_pyr_30, r_pyr_31);
    hls::Resize(r_pyr_30, r_pyr_4);

    // green pyramid
    GRAY_IMAGE g_pyr_00(rows,cols),g_pyr_01(rows,cols);
    GRAY_IMAGE g_pyr_1(rows/2, cols/2), g_pyr_10(rows/2, cols/2), g_pyr_11(rows/2, cols/2);
    GRAY_IMAGE g_pyr_2(rows/4, cols/4), g_pyr_20(rows/4, cols/4), g_pyr_21(rows/4, cols/4);
    GRAY_IMAGE g_pyr_3(rows/8, cols/8), g_pyr_30(rows/8, cols/8), g_pyr_31(rows/8, cols/8);
    GRAY_IMAGE g_pyr_4(rows/16, cols/16);

    GRAY_IMAGE g_blur_img(rows, cols);

    hls::GaussianBlur<3,3>(green, g_blur_img);

    hls::Duplicate(g_blur_img, g_pyr_00,g_pyr_01);
    hls::Resize(g_pyr_00, g_pyr_1);
    hls::Duplicate(g_pyr_1, g_pyr_10, g_pyr_11);
    hls::Resize(g_pyr_10, g_pyr_2);
    hls::Duplicate(g_pyr_2, g_pyr_20, g_pyr_21);
    hls::Resize(g_pyr_20, g_pyr_3);
    hls::Duplicate(g_pyr_3, g_pyr_30, g_pyr_31);
    hls::Resize(g_pyr_30, g_pyr_4);

    GRAY_IMAGE      rg_diff_0(rows, cols);
    GRAY_IMAGE  rg_diff_1(rows/2, cols/2);
    GRAY_IMAGE  gr_diff_2(rows/4, cols/4);
    GRAY_IMAGE  gr_diff_3(rows/8, cols/8);
    GRAY_IMAGE  gr_diff_4(rows/16, cols/16);

    // find diff
    hls::AbsDiff(r_pyr_01, g_pyr_01, rg_diff_0);
    hls::AbsDiff(r_pyr_11, g_pyr_11, rg_diff_1);
    hls::AbsDiff(g_pyr_21, r_pyr_21, gr_diff_2);
    hls::AbsDiff(g_pyr_31, r_pyr_31, gr_diff_3);
    hls::AbsDiff(g_pyr_4,  r_pyr_4,  gr_diff_4);

    GRAY_IMAGE      map_02(rows, cols), map_03(rows, cols), map_02_03(rows, cols), rg_diff_00(rows, cols), rg_diff_01(rows, cols), r_00(rows, cols), r_01(rows, cols);
    GRAY_IMAGE      map_13(rows/2 ,cols/2), map_14(rows/2 ,cols/2), map_13_14(rows/2 ,cols/2), map_1(rows/2 ,cols/2), rg_diff_10(rows/2 ,cols/2), rg_diff_11(rows/2 ,cols/2), r_10(rows/2 ,cols/2), r_11(rows/2 ,cols/2);
    GRAY_IMAGE      gr_diff_30(rows/8, cols/8), gr_diff_31(rows/8, cols/8);
    // center surround
    hls::Duplicate(rg_diff_0, rg_diff_00, rg_diff_01);
    hls::Duplicate(rg_diff_1, rg_diff_10, rg_diff_11);
    hls::Duplicate(gr_diff_3, gr_diff_30, gr_diff_31);

    hls::Resize(gr_diff_2, r_00);
    hls::AbsDiff(r_00, rg_diff_00, map_02);

    hls::Resize(gr_diff_30, r_01);
    hls::AbsDiff(r_01, rg_diff_01, map_03);

     hls::Resize(gr_diff_31, r_10);
     hls::AbsDiff(r_10, rg_diff_10, map_13);
     hls::Resize(gr_diff_4, r_11);
     hls::AbsDiff(r_11, rg_diff_11, map_14);

     // combine maps
     hls::AddWeighted(map_02, 0.5, map_03, 0.5, 0.0, map_02_03);
     hls::AddWeighted(map_13, 0.5, map_14, 0.5, 0.0, map_13_14);

     hls::Resize(map_02_03, map_1);

     hls::AddWeighted(map_1, 0.5, map_13_14, 0.5, 0.0, rg_map);

}
