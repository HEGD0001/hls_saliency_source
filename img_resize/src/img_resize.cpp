
  #include "hls_video.h"

  // maximum image size
  #define MAX_WIDTH  1920
  #define MAX_HEIGHT 1080
typedef hls::stream<ap_axiu<32,1,1,1> >               AXI_STREAM;
typedef hls::Mat<MAX_HEIGHT, MAX_WIDTH, HLS_8UC1>         GRAY_IMAGE;

// HW center-surround : center is 2 level down in the pyramid
  void hw_img_resize(AXI_STREAM& input, AXI_STREAM& output, int in_rows, int in_cols, int out_rows, int out_cols)

  {

       //Create AXI streaming interfaces for the core
   #pragma HLS INTERFACE axis port=input
   #pragma HLS INTERFACE axis port=output

  #pragma HLS RESOURCE core=AXI_SLAVE variable=in_rows metadata="-bus_bundle CONTROL_BUS"
  #pragma HLS RESOURCE core=AXI_SLAVE variable=in_cols metadata="-bus_bundle CONTROL_BUS"
  #pragma HLS RESOURCE core=AXI_SLAVE variable=out_rows metadata="-bus_bundle CONTROL_BUS"
  #pragma HLS RESOURCE core=AXI_SLAVE variable=out_cols metadata="-bus_bundle CONTROL_BUS"

  #pragma HLS RESOURCE core=AXI_SLAVE variable=return metadata="-bus_bundle CONTROL_BUS"

  #pragma HLS INTERFACE ap_stable port=in_rows
  #pragma HLS INTERFACE ap_stable port=in_cols
  #pragma HLS INTERFACE ap_stable port=out_rows
  #pragma HLS INTERFACE ap_stable port=out_cols
      GRAY_IMAGE in_img(in_rows, in_cols);
      GRAY_IMAGE out_img(out_rows, out_cols);
  #pragma HLS dataflow

      // Convert input image stream to Mat
     hls::AXIvideo2Mat(input, in_img);
     hls::Resize<HLS_8UC1>(in_img, out_img);
     hls::Mat2AXIvideo(out_img, output);
  }

