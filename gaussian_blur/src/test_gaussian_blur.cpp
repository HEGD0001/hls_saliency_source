#include "gaussian_blur.h"
#include "hls_opencv.h"

#define INPUT_IMG   "test_img.jpg"

int main(int argc, char** argv)
{
    // load input image
    IplImage * in_img = cvLoadImage(INPUT_IMG);

    IplImage * frame_r = cvCreateImage(cvGetSize(in_img), HLS_8U, 1);
    IplImage * frame_g = cvCreateImage(cvGetSize(in_img), HLS_8U, 1);
    IplImage * frame_b = cvCreateImage(cvGetSize(in_img), HLS_8U, 1);
    IplImage * out_img = cvCreateImage(cvGetSize(in_img), HLS_8U, 1);

    cvSplit(in_img, frame_r, frame_g, frame_b, NULL);

    AXI_STREAM red_axi, dst_axi;
    IplImage2AXIvideo(frame_r, red_axi);

    // red channel is blurred for testing
    hw_gaussian_blur(red_axi, dst_axi, in_img->height, in_img->width);

    AXIvideo2IplImage(dst_axi, out_img);

    cvSaveImage("gaussian_blurred_red.jpg", out_img);

    cvReleaseImage(&frame_r);
    cvReleaseImage(&frame_g);
    cvReleaseImage(&frame_b);
    cvReleaseImage(&out_img);

    return 0;
}
