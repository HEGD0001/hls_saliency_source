#ifndef _IMG_GAUSSIAN_BLUR_H_
#define _IMG_GAUSSIAN_BLUR_H_

#include "hls_video.h"
#define MAX_HEIGHT  1080
#define MAX_WIDTH   1920


 typedef hls::stream<ap_axiu<32,1,1,1> >               AXI_STREAM;
 typedef hls::Scalar<3, unsigned char>                 RGB_PIXEL;
 typedef hls::Mat<MAX_HEIGHT, MAX_WIDTH, HLS_32SC1>    GRAY_IMAGE;


void hw_gaussian_blur(AXI_STREAM& input, AXI_STREAM& output, int rows, int cols);



#endif // _IMG_GAUSSIAN_BLUR_H_
