#include "gaussian_blur.h"

void hw_gaussian_blur(AXI_STREAM& input, AXI_STREAM& output, int rows, int cols)
{

       //Create AXI streaming interfaces for the core
 #pragma HLS RESOURCE variable=input core=AXIS metadata="-bus_bundle INPUT_STREAM"
 #pragma HLS RESOURCE variable=output core=AXIS metadata="-bus_bundle OUTPUT_STREAM"

 #pragma HLS RESOURCE core=AXI_SLAVE variable=rows metadata="-bus_bundle CONTROL_BUS"
 #pragma HLS RESOURCE core=AXI_SLAVE variable=cols metadata="-bus_bundle CONTROL_BUS"
 #pragma HLS RESOURCE core=AXI_SLAVE variable=return metadata="-bus_bundle CONTROL_BUS"

 #pragma HLS INTERFACE ap_stable port=rows
 #pragma HLS INTERFACE ap_stable port=cols

    GRAY_IMAGE input_img(rows, cols);
    GRAY_IMAGE out_img(rows, cols);

 #pragma HLS dataflow
    // Convert input image stream to Mat
    hls::AXIvideo2Mat(input, input_img);
    hls::GaussianBlur<3,3>(input_img, out_img);
    hls::Mat2AXIvideo(out_img, output);
}
