
# Create the design project
open_project hls_ip -reset

# Define the top level function for hardware synthesis
set_top hw_gaussian_blur

# Select the files for hardware synthesis
add_files ./src/gaussian_blur.cpp
add_files ./src/gaussian_blur.h

# Select the files required for the testbench
add_files -tb ./src/test_gaussian_blur.cpp
add_files -tb ./src/test_img.bmp

# Set the name of the solution for this design
open_solution "solution"

# Select the FPGA 
set_part "xc7z020clg484-1"
create_clock -period 10 -name default

# Set flags
set csim_flag 1
if { [info exists ::env(csim_flag)] } {
    set csim_flag $::env(csim_flag)
}
set csynth_flag 1
if { [info exists ::env(csynth_flag)] } {
    set csynth_flag $::env(csynth_flag)
}
set cosim_flag 0
if { [info exists ::env(cosim_flag)] } {
    set cosim_flag $::env(cosim_flag)
}
set export_flag 1
if { [info exists ::env(export_flag)] } {
    set export_flag $::env(export_flag)
}

if { $csim_flag == 1 } {
    # Vivado HLS commands for C simulation
    csim_design -mflags -j4 -O
}

if { $csynth_flag == 1 } {
    # Vivado HLS commands for C to RTL synthesis
    csynth_design
}

if { $cosim_flag == 1 } {
    # Vivado HLS commands for RTL simulation
    cosim_design
}

if { $export_flag == 1 } {
    # Vivado HLS commands for RTL implementation
    export_design -format ip_catalog -description "Image addition HW IP" -vendor "ntu" -library "opencv" -version "1.0"
}

exit
