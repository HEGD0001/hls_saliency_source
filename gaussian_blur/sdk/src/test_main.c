/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* XILINX CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "platform.h"
#include "xVDMA.h"
#include "xhw_gaussian_blur.h"
#include "xil_cache.h"
#include "xtime_l.h"
#define timer_base 0xF8F00000

#define INPUT_FRAME_ADDR 0x10000000
#define OUTPUT_FRAME_ADDR 0x10100000 

// OCM addresses
//#define INPUT_FRAME_ADDR 0x00000000
//#define OUTPUT_FRAME_ADDR 0x00000000 
#define HLS_ADDR 0x43000000

//VDMA setup parameters
#define VDMAPixelWidth 4
#define VDMABaseAddr XPAR_AXI_VDMA_0_BASEADDR
#define MAX_ROWS	720
#define MAX_COLS	1080

/***********************************************************
Timer Registers
************************************************************/
static volatile int *timer_counter_l=(volatile int *)(timer_base+0x200);
static volatile int *timer_counter_h=(volatile int *)(timer_base+0x204);
static volatile int *timer_ctrl=(volatile int *)(timer_base+0x208);
/***********************************************************
***********************************************************
Function definitions
************************************************************/
void init_timer(volatile int *timer_ctrl, volatile int *timer_counter_l, volatile int *timer_counter_h){
        *timer_ctrl=0x0;
        *timer_counter_l=0x1;
        *timer_counter_h=0x0;
        DATA_SYNC;
}

void start_timer(volatile int *timer_ctrl){
        *timer_ctrl=*timer_ctrl | 0x00000001;
        DATA_SYNC;
}

void stop_timer(volatile int *timer_ctrl){
        *timer_ctrl=*timer_ctrl & 0xFFFFFFFE;
        DATA_SYNC;
}
XHw_gaussian_blur setup_ximage_filter(uint input_address);

int main()
{
	int row, col;
	XHw_gaussian_blur HLSDevice;
    xVDMA_info vdma;

    // Setup image
    u32 * in_img = (u32 *)INPUT_FRAME_ADDR;
    u32 * out_img = (u32 *)OUTPUT_FRAME_ADDR;

    for (row = 0; row < MAX_ROWS; row++)
    {
    	for (col = 0; col < MAX_COLS; col++)
    	{
    		if (row % 2 == 0 )
    		{
    			*(in_img + row * MAX_COLS + col) = 200;
    		}
    		else
    		{
    			*(in_img + row * MAX_COLS + col) = 150;
    		}
    	}
    }

    init_platform();
	//Xil_SetTlbAttributes(0x00000000,0x14de2);
    //Xil_DCacheDisable();


    printf("HLS IP init\n");
    HLSDevice = setup_ximage_filter(HLS_ADDR);

    XHw_gaussian_blur_SetRows(&HLSDevice, MAX_ROWS);
    XHw_gaussian_blur_SetCols(&HLSDevice, MAX_COLS);

    xVDMA_Init(&vdma, VDMABaseAddr, MAX_COLS, MAX_ROWS, VDMAPixelWidth);

    Xil_DCacheFlushRange(INPUT_FRAME_ADDR, VDMAPixelWidth*MAX_ROWS*MAX_COLS);

    printf("VDMA init done\n");

	init_timer(timer_ctrl, timer_counter_l, timer_counter_h);
    start_timer(timer_ctrl);

    XHw_gaussian_blur_Start(&HLSDevice); //Kick it to start it
    xVDMA_kick(&vdma, INPUT_FRAME_ADDR, OUTPUT_FRAME_ADDR);
	
	// wait for completion signal from HW
    while(!XHw_gaussian_blur_IsDone(&HLSDevice)){}

	// Wait for transfer to complete
    while(!xVDMA_IsDone(&vdma)){}

    stop_timer(timer_ctrl);
    xil_printf("Total time per frame =  %d us\n\r", (*timer_counter_l)/333);

    printf("image filtering is done.....................\n");
    Xil_DCacheInvalidateRange(OUTPUT_FRAME_ADDR, VDMAPixelWidth*MAX_ROWS*MAX_COLS);

    printf("%d %d\n", out_img[10], out_img[MAX_COLS]);
    cleanup_platform();
    return 0;
}



XHw_gaussian_blur setup_ximage_filter(uint input_address)
{

	XHw_gaussian_blur device;
    device.Control_bus_BaseAddress = input_address;

    device.IsReady = XIL_COMPONENT_IS_READY;

    return device;
}
