#include "hls_opencv.h"
#include "intensity_map.h"
#define INPUT_IMAGE "test_img.jpg"

int main (int argc, char** argv) {


    IplImage* in_img = cvLoadImage(INPUT_IMAGE);


    IplImage* int_map = cvCreateImage(cvSize(in_img->width/2, in_img->height/2), in_img->depth, 1);
    IplImage* gray_img = cvCreateImage(cvGetSize(in_img), in_img->depth, 1);

	cvCvtColor(in_img, gray_img, CV_RGB2GRAY);

    AXI_STREAM  src_axi, out_axi;
    IplImage2AXIvideo(gray_img, src_axi);

    hw_intensity_map(src_axi, out_axi, in_img->height, in_img->width);

    AXIvideo2IplImage(out_axi, int_map);

    cvSaveImage("intensity_map.jpg", int_map);
    cvReleaseImage(&int_map);
    cvReleaseImage(&gray_img);
	return 0;

}
