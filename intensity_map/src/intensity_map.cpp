#include "intensity_map.h"


void hw_intensity_map(AXI_STREAM& int_axi, AXI_STREAM& int_map_axi, int rows, int cols)
{
#pragma HLS INTERFACE axis port=int_axi
#pragma HLS INTERFACE axis port=int_map_axi

#pragma HLS RESOURCE core=AXI_SLAVE variable=rows metadata="-bus_bundle CONTROL_BUS"
#pragma HLS RESOURCE core=AXI_SLAVE variable=cols metadata="-bus_bundle CONTROL_BUS"
#pragma HLS RESOURCE core=AXI_SLAVE variable=return metadata="-bus_bundle CONTROL_BUS"

#pragma HLS INTERFACE ap_stable port=rows
#pragma HLS INTERFACE ap_stable port=cols

#pragma HLS dataflow

	GRAY_IMAGE int_img(rows,cols);
	GRAY_IMAGE int_map(rows/2, cols/2);

	hls::AXIvideo2Mat(int_axi, int_img);

	GRAY_IMAGE pyr_00(rows,cols), pyr_01(rows,cols),pyr_02(rows,cols),pyr_03(rows,cols), r_00(rows,cols),r_01(rows,cols);
	GRAY_IMAGE pyr_1(rows/2, cols/2), pyr_10(rows/2, cols/2), pyr_11(rows/2, cols/2), pyr_12(rows/2, cols/2), pyr_13(rows/2, cols/2), r_10(rows/2, cols/2), r_11(rows/2, cols/2);
	GRAY_IMAGE	pyr_2(rows/4, cols/4), pyr_20(rows/4, cols/4), pyr_21(rows/4, cols/4);
	GRAY_IMAGE pyr_3(rows/8, cols/8), pyr_30(rows/8, cols/8), pyr_31(rows/8, cols/8), pyr_32(rows/8, cols/8), pyr_33(rows/8, cols/8);
	GRAY_IMAGE pyr_4(rows/16,cols/16);

	GRAY_IMAGE blur_img(rows,cols);

	hls::GaussianBlur<3,3>(int_img, blur_img);

	hls::Duplicate(blur_img, pyr_00,pyr_01);
	hls::Resize(pyr_00, pyr_1);
	hls::Duplicate(pyr_1, pyr_10, pyr_11);
	hls::Resize(pyr_10, pyr_2);
	hls::Duplicate(pyr_2, pyr_20, pyr_21);
	hls::Resize(pyr_20, pyr_3);
	hls::Duplicate(pyr_3, pyr_30, pyr_31);
	hls::Resize(pyr_30, pyr_4);

	// find CS difference and intensity map
	GRAY_IMAGE map_02(rows,cols), map_03(rows,cols),map_02_03(rows,cols);
	GRAY_IMAGE map_13(rows/2,cols/2), map_14(rows/2,cols/2),map_1(rows/2,cols/2), map_13_14(rows/2,cols/2);

	hls::Resize(pyr_21, r_00);
	hls::Duplicate(pyr_01, pyr_02, pyr_03);
	hls::AbsDiff(r_00, pyr_02,map_02);

	hls::Duplicate(pyr_31, pyr_32, pyr_33);
	hls::Resize(pyr_32, r_01);
	hls::AbsDiff(r_01, pyr_03, map_03);

	hls::Resize(pyr_33, r_10);
	hls::Duplicate(pyr_11, pyr_12, pyr_13);
	hls::AbsDiff(r_10, pyr_12, map_13);

	hls::Resize(pyr_4, r_11);
	hls::AbsDiff(r_11,pyr_13,  map_14);


	// combine maps
	hls::AddWeighted(map_02, 0.5, map_03, 0.5, 0.0, map_02_03);
	hls::AddWeighted(map_13, 0.5, map_14, 0.5, 0.0, map_13_14);

	hls::Resize(map_02_03, map_1);

	hls::AddWeighted(map_1, 0.5, map_13_14, 0.5, 0.0, int_map);
	hls::Mat2AXIvideo(int_map, int_map_axi);
}
