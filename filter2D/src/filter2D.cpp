#include "filter2D.h"

#define MAX_HEIGHT  1080
#define MAX_WIDTH   1920



void hw_filter2D(AXI_STREAM& input, AXI_STREAM& output, int in_rows, int in_cols)
{

       //Create AXI streaming interfaces for the core
#pragma HLS INTERFACE axis port=input
#pragma HLS INTERFACE axis port=output

#pragma HLS RESOURCE core=AXI_SLAVE variable=in_rows metadata="-bus_bundle CONTROL_BUS"
#pragma HLS RESOURCE core=AXI_SLAVE variable=in_cols metadata="-bus_bundle CONTROL_BUS"
#pragma HLS RESOURCE core=AXI_SLAVE variable=return metadata="-bus_bundle CONTROL_BUS"

#pragma HLS INTERFACE ap_stable port=in_rows
#pragma HLS INTERFACE ap_stable port=in_cols

    GRAY_IMAGE input_img(in_rows, in_cols);
    GRAY_IMAGE out_img(in_rows, in_cols);

    // Convert input image stream to Mat
    hls::AXIvideo2Mat(input, input_img);
    hls::GaussianBlur<3,3>(input_img, out_img);
    hls::Mat2AXIvideo(out_img, output);
}
