/***************************************************************************
Author: Gopalakrishna Hegde

***************************************************************************/

#ifndef _HW_PYRAMID_FILTER_H_
#define _HW_PYRAMID_FILTER_H_

#include "hls_video.h"

// maximum image size
#define MAX_WIDTH  1920
#define MAX_HEIGHT 1080

// I/O Image Settings
#define INPUT_IMAGE           "test_1080p.bmp"
#define OUTPUT_IMAGE          "result_1080p.bmp"
#define OUTPUT_IMAGE_GOLDEN   "result_1080p_golden.bmp"

// typedef video library core structures
typedef hls::stream<ap_axiu<32,1,1,1> >               AXI_STREAM;
//typedef hls::Mat<MAX_HEIGHT, MAX_WIDTH, HLS_8UC3>     RGB_IMAGE;
typedef hls::Mat<MAX_HEIGHT, MAX_WIDTH, HLS_8UC1>         GRAY_IMAGE;
//typedef hls::Mat<MAX_HEIGHT/2, MAX_WIDTH/2, HLS_8UC1>     GRAY_IMAGE_2;
//typedef hls::Mat<MAX_HEIGHT/4, MAX_WIDTH/4, HLS_8UC1>     GRAY_IMAGE_4;
//typedef hls::Mat<MAX_HEIGHT/8, MAX_WIDTH/8, HLS_8UC1>     GRAY_IMAGE_8;
//typedef hls::Mat<MAX_HEIGHT/16, MAX_WIDTH/16, HLS_8UC1>   GRAY_IMAGE_16;

// top level function for HW synthesis
void hw_pyramid_filter(AXI_STREAM& INPUT_STREAM, AXI_STREAM& PYR_0, AXI_STREAM& PYR_1,
AXI_STREAM& PYR_2, AXI_STREAM& PYR_3, AXI_STREAM& PYR_4,int rows, int cols);

#endif
