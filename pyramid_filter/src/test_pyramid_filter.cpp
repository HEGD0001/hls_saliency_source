/**********************************************************************/
#include "pyramid_filter.h"
#include "hls_opencv.h"

int main (int argc, char** argv) {

    CvSize pyr_0_size,pyr_1_size,pyr_2_size,pyr_3_size,pyr_4_size;



    IplImage* src = cvLoadImage(INPUT_IMAGE);
    pyr_0_size.height = src->height;	pyr_0_size.width = src->width;
    pyr_1_size.height = src->height/2;	pyr_1_size.width = src->width/2;
    pyr_2_size.height = src->height/4;	pyr_2_size.width = src->width/4;
    pyr_3_size.height = src->height/8;	pyr_3_size.width = src->width/8;
    pyr_4_size.height = src->height/16;	pyr_4_size.width = src->width/16;

    IplImage* pyr_0 = cvCreateImage(pyr_0_size, src->depth, 1);
    IplImage* pyr_1 = cvCreateImage(pyr_1_size, src->depth, 1);
    IplImage* pyr_2 = cvCreateImage(pyr_2_size, src->depth, 1);
    IplImage* pyr_3 = cvCreateImage(pyr_3_size, src->depth, 1);
    IplImage* pyr_4 = cvCreateImage(pyr_4_size, src->depth, 1);

    AXI_STREAM  src_axi, pyr_0_axi,  pyr_1_axi, pyr_2_axi, pyr_3_axi, pyr_4_axi;
    IplImage2AXIvideo(src, src_axi);

    hw_pyramid_filter(src_axi,  pyr_0_axi, pyr_1_axi, pyr_2_axi, pyr_3_axi, pyr_4_axi, src->height, src->width);

    AXIvideo2IplImage(pyr_0_axi, pyr_0);
    AXIvideo2IplImage(pyr_1_axi, pyr_1);
    AXIvideo2IplImage(pyr_2_axi, pyr_2);
    AXIvideo2IplImage(pyr_3_axi, pyr_3);
    AXIvideo2IplImage(pyr_4_axi, pyr_4);
    cvSaveImage("hw_pyr_0.bmp", pyr_0);
    cvSaveImage("hw_pyr_1.bmp", pyr_1);
    cvSaveImage("hw_pyr_2.bmp", pyr_2);
    cvSaveImage("hw_pyr_3.bmp", pyr_3);
    cvSaveImage("hw_pyr_4.bmp", pyr_4);


    cvReleaseImage(&src);
    cvReleaseImage(&pyr_0);
    cvReleaseImage(&pyr_1);
    cvReleaseImage(&pyr_2);
    cvReleaseImage(&pyr_3);
    cvReleaseImage(&pyr_4);
    return 0;

}
