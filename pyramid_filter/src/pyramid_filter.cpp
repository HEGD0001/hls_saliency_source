#include "pyramid_filter.h"

// HW pyramid filter of depth 5
void hw_pyramid_filter(AXI_STREAM& INPUT_STREAM, AXI_STREAM& PYR_0, AXI_STREAM& PYR_1,
AXI_STREAM& PYR_2, AXI_STREAM& PYR_3, AXI_STREAM& PYR_4, int rows, int cols)
{

    //Create AXI streaming interfaces for the core
#pragma HLS INTERFACE axis port=INPUT_STREAM
#pragma HLS INTERFACE axis port=PYR_0
#pragma HLS INTERFACE axis port=PYR_1
#pragma HLS INTERFACE axis port=PYR_2
#pragma HLS INTERFACE axis port=PYR_3
#pragma HLS INTERFACE axis port=PYR_4

#pragma HLS RESOURCE core=AXI_SLAVE variable=rows metadata="-bus_bundle CONTROL_BUS"
#pragma HLS RESOURCE core=AXI_SLAVE variable=cols metadata="-bus_bundle CONTROL_BUS"
#pragma HLS RESOURCE core=AXI_SLAVE variable=return metadata="-bus_bundle CONTROL_BUS"

#pragma HLS INTERFACE ap_stable port=rows
#pragma HLS INTERFACE ap_stable port=cols

    GRAY_IMAGE in_img(rows, cols);
#pragma HLS dataflow

    // Convert input image stream to Mat
    hls::AXIvideo2Mat(INPUT_STREAM, in_img);

    GRAY_IMAGE pyr_00(rows, cols);
    GRAY_IMAGE pyr_01(rows,cols);
    GRAY_IMAGE pyr_1(rows/2,cols/2); GRAY_IMAGE pyr_10(rows/2,cols/2); GRAY_IMAGE pyr_11(rows/2,cols/2);
    GRAY_IMAGE pyr_2(rows/4, cols/4); GRAY_IMAGE pyr_20(rows/4,cols/4); GRAY_IMAGE pyr_21(rows/4,cols/4);
    GRAY_IMAGE pyr_3(rows/8,cols/8); GRAY_IMAGE pyr_30(rows/8, cols/8); GRAY_IMAGE pyr_31(rows/8, cols/8);
    GRAY_IMAGE pyr_4(rows/16, cols/16);

    GRAY_IMAGE blur_img(rows,cols);

    hls::GaussianBlur<3,3>(in_img, blur_img);
    hls::Duplicate(blur_img, pyr_00,pyr_01);
    hls::Resize<HLS_8UC1>(pyr_00, pyr_1);
    hls::Duplicate(pyr_1, pyr_10, pyr_11);
    hls::Resize<HLS_8UC1>(pyr_10, pyr_2);
    hls::Duplicate(pyr_2, pyr_20, pyr_21);
    hls::Resize<HLS_8UC1>(pyr_20, pyr_3);
    hls::Duplicate(pyr_3, pyr_30, pyr_31);
    hls::Resize<HLS_8UC1>(pyr_30, pyr_4);

    // stream the result out
    hls::Mat2AXIvideo(pyr_01, PYR_0);
    hls::Mat2AXIvideo(pyr_11, PYR_1);
    hls::Mat2AXIvideo(pyr_21, PYR_2);
    hls::Mat2AXIvideo(pyr_31, PYR_3);
    hls::Mat2AXIvideo(pyr_4, PYR_4);
}

