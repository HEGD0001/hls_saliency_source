#include "img_add.h"
#include "hls_opencv.h"

#define INPUT_IMG	"test_img.jpg"

int main(int argc, char** argv)
{
	// we will add red and green channels here
	IplImage * in_img = cvLoadImage(INPUT_IMG);

	IplImage * frame_r = cvCreateImage(cvGetSize(in_img), HLS_8U, 1);
	IplImage * frame_g = cvCreateImage(cvGetSize(in_img), HLS_8U, 1);
	IplImage * frame_b = cvCreateImage(cvGetSize(in_img), HLS_8U, 1);
	IplImage * out_img = cvCreateImage(cvGetSize(in_img), HLS_8U, 1);

	cvSplit(in_img, frame_r, frame_g, frame_b, NULL);

	AXI_STREAM src1_axi, src2_axi, dst_axi;
	IplImage2AXIvideo(frame_r, src1_axi);
	IplImage2AXIvideo(frame_g, src2_axi);

	hw_img_add(src1_axi, src2_axi, dst_axi, in_img->height, in_img->width);

	AXIvideo2IplImage(dst_axi, out_img);

	cvSaveImage("out_image_sum.jpg", out_img);

	cvReleaseImage(&frame_r);
	cvReleaseImage(&frame_g);
	cvReleaseImage(&frame_b);
	cvReleaseImage(&out_img);

	return 0;
}
