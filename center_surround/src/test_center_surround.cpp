#include "center_surround.h"
#include "hls_opencv.h"

#define INPUT_IMG   "test_img.jpg"

int main(int argc, char** argv)
{
    // we will subtract red and green channels here
    IplImage * in_img = cvLoadImage(INPUT_IMG);

    IplImage * frame_r = cvCreateImage(cvGetSize(in_img), HLS_8U, 1);
    IplImage * frame_g = cvCreateImage(cvGetSize(in_img), HLS_8U, 1);
    IplImage * frame_b = cvCreateImage(cvGetSize(in_img), HLS_8U, 1);
    IplImage * out_img = cvCreateImage(cvGetSize(in_img), HLS_8U, 1);

    cvSplit(in_img, frame_r, frame_g, frame_b, NULL);

    IplImage *resized_img = cvCreateImage(cvSize(in_img->width/2, in_img->height/2), frame_r->depth, frame_r->nChannels);
    cvResize(frame_r, resized_img);
    AXI_STREAM src1_axi, src2_axi, dst_axi;

    IplImage2AXIvideo(frame_r, src1_axi);
    IplImage2AXIvideo(resized_img, src2_axi);

    hw_center_surround(src1_axi, src2_axi, dst_axi, frame_r->height, frame_r->width, resized_img->height, resized_img->width);

    AXIvideo2IplImage(dst_axi, out_img);

    cvSaveImage("center_surround_out.jpg", out_img);

    cvReleaseImage(&frame_r);
    cvReleaseImage(&frame_g);
    cvReleaseImage(&frame_b);
    cvReleaseImage(&out_img);

    return 0;
}
