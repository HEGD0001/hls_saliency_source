#ifndef _IMG_CENTER_SURROUND_H_
#define _IMG_CENTER_SURROUND_H_

#include "hls_video.h"
#define MAX_HEIGHT  1080
#define MAX_WIDTH   1920


typedef hls::Mat<MAX_HEIGHT, MAX_WIDTH, HLS_8UC1>     GRAY_IMAGE;
typedef hls::stream<ap_axiu<32,1,1,1> >               AXI_STREAM;


void hw_center_surround(AXI_STREAM& center, AXI_STREAM& surround, AXI_STREAM& cs_out, int s_rows, int s_cols, int c_rows, int  c_cols);



#endif // _IMG_CENTER_SURROUND_H_
