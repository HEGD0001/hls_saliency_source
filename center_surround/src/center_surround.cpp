#include "center_surround.h"

void hw_center_surround(AXI_STREAM& center, AXI_STREAM& surround, AXI_STREAM& cs_out, int s_rows, int s_cols, int c_rows, int  c_cols)
{

//Create AXI streaming interfaces for the core
#pragma HLS INTERFACE axis port=center
#pragma HLS INTERFACE axis port=surround
#pragma HLS INTERFACE axis port=cs_out

#pragma HLS RESOURCE core=AXI_SLAVE variable=s_rows metadata="-bus_bundle CONTROL_BUS"
#pragma HLS RESOURCE core=AXI_SLAVE variable=s_cols metadata="-bus_bundle CONTROL_BUS"
#pragma HLS RESOURCE core=AXI_SLAVE variable=c_rows metadata="-bus_bundle CONTROL_BUS"
#pragma HLS RESOURCE core=AXI_SLAVE variable=c_cols metadata="-bus_bundle CONTROL_BUS"
#pragma HLS RESOURCE core=AXI_SLAVE variable=return metadata="-bus_bundle CONTROL_BUS"

#pragma HLS INTERFACE ap_stable port=s_rows
#pragma HLS INTERFACE ap_stable port=s_cols

    GRAY_IMAGE s_img(s_rows, s_cols);
    GRAY_IMAGE c_img(c_rows, c_cols);
    GRAY_IMAGE cs_img(s_rows, s_cols);
    GRAY_IMAGE resized_img(s_rows, s_cols);

    // Convert input image stream to Mat
    hls::AXIvideo2Mat(center, c_img);
    hls::AXIvideo2Mat(surround, s_img);
    hls::Resize(c_img, resized_img);

    hls::AbsDiff(resized_img, s_img, cs_img);
    hls::Mat2AXIvideo(cs_img, cs_out);
}
